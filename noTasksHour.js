const cronJob = require("cron").CronJob;
const { loggerNoTask } = require("./logger");
const { DateTime } = require('luxon');

const { headers, url, client, DEFAULT_NAME, MIN_DATE_OBJ } = require("./constants");
const { noTaskHour } = require("./mapping");
const Utils = require("./request");
const optionsEmpInf = Utils.makeBaseOption("Employee/GetAllFullEmployee");
const optionsCompGroup = Utils.makeBaseOption("CompanyGroup/GetFlatCompanyGroupsVersion2", {});

let countTask = 0;
let history = true;
  
// Usage:
async function main() {
    try {
        let thisDay = Utils.getFirstorLastDayofWeek();
        let body = {
            "fromDate": thisDay.text,
            "toDate": thisDay.text,
            "originalGroupID": [],
            "projectID": null,
            "featureID": null,
            "workTypeID": null
        };
        if (history) {
            thisDay.date = thisDay.date.minus({ days: 1 });
            body = {
                "fromDate": "2019-12-5",
                "toDate": thisDay.date.year + "-" + thisDay.date.month + "-" + thisDay.date.day,
                "originalGroupID": [],
                "projectID": null,
                "featureID": null,
                "workTypeID": null
            };
        }

        const optionsWb = Utils.makeBaseOption("WorkBlock/GetWorkBlocks", body);
        let resWb = await Utils.doRequest(optionsWb);

        let listHoliday = [];
        for (let i = 2019; i <= DateTime.local().year; i++) {
            let optionsHoliday = Utils.makeBaseOption("holiday/GetAll?year=" + i, null, "GET");
            let resHoliday = await Utils.doRequest(optionsHoliday);
            listHoliday = listHoliday.concat(Array.from(JSON.parse(resHoliday)));
        }
        // let resProInf = await Utils.doRequest(optionsProInf);
        let resEmpInf = await Utils.doRequest(optionsEmpInf);
        // let resFieldConf = await Utils.doRequest(optionsFieldConf);
        let resCompGroup = await Utils.doRequest(optionsCompGroup);

        let response = JSON.parse(resWb);
        // const projectInfo = JSON.parse(resProInf);
        const empInfo = JSON.parse(resEmpInf);
        // const fieldConf = JSON.parse(resFieldConf);
        // const workTypeList = fieldConf.find(o => o.fieldName === "WorkTypeID");
        const compGroup = JSON.parse(resCompGroup);
        let newEmployeeArray = [];

        const listWbReport = response.wBlocks.filter(o => o.type === 0 || o.type === 1);
        let dayWb = [];
        listWbReport && listWbReport.forEach(wb => {
            const date = DateTime.fromISO(wb.endDate);
            const objectDate = {
                date: date.day,
                month: date.month,
                year: date.year
            }
            if (!dayWb.some(o => o.date === objectDate.date && o.month === objectDate.month && o.year === objectDate.year)) {
                dayWb.push(objectDate);
            }
        });

        dayWb.forEach(day => {
            let checkHoliday = false;
            listHoliday.forEach(holiday => {
                let holidayDate = DateTime.fromISO(holiday.date);
                if (holidayDate.day === day.date && holidayDate.month === day.month && holidayDate.year === day.year) {
                    checkHoliday = true;
                }
            });
            if (!checkHoliday) {
                response.employeeIDs.map(obj => {
                    let partName = DEFAULT_NAME;
                    let teamName = DEFAULT_NAME;
                    const indexEmpFound = empInfo.findIndex(o => o.employeeID === obj);
                    const listWbEmp = listWbReport.filter(o => o.assigneeID === obj);
        
                    const indexCompGroup = compGroup ?
                        compGroup.companyGroups.findIndex(o => o.employees.some(emp => emp.employeeID === obj)) :
                        -1;
                    if (indexCompGroup >= 0) {
                        const groupInfo = compGroup.companyGroups[indexCompGroup];
                        if (groupInfo.levelName.toLowerCase() === "part") {
                            partName = groupInfo.groupName;
                            const indexTeam = compGroup.companyGroups.findIndex(o => o.oldGroupID === groupInfo.parentGroupID);
                            if (indexTeam >= 0) {
                                teamName = compGroup.companyGroups[indexTeam].groupName;
                            }
                        } else if (groupInfo.levelName.toLowerCase() === "team") {
                            teamName = groupInfo.groupName;
                        }
                    }
                    const reducer = (accumulator, currentValue) => {
                        const dateInWb = DateTime.fromISO(currentValue.endDate);
                        if (dateInWb.day === day.date && dateInWb.month === day.month && dateInWb.year === day.year) {
                            accumulator +=  currentValue.hours;
                        }
                        return accumulator;
                    }
                    day.totalHour = listWbEmp && listWbEmp.length > 0 ? listWbEmp.reduce(reducer, 0) : 0;
                    if (indexEmpFound >= 0) {
                        const joinDate = DateTime.fromISO(empInfo[indexEmpFound].dateOfJoin);
                        const dataDay = DateTime.local(day.year, day.month, day.date);
                        if (dataDay.startOf("day") >= joinDate.startOf("day")) {
                            newEmployeeArray.push({
                                id: obj + day.date + day.month + day.year,
                                name: empInfo[indexEmpFound] ? empInfo[indexEmpFound].englishName : "",
                                partName: partName,
                                teamName: teamName,
                                workingDay: dataDay.endOf("day").toString(),
                                noTaskHour: 8 - day.totalHour >= 0 ? 8 - day.totalHour : 0
                            });
                        }
                    }
                });
            }
        });

        loggerNoTask.info("Prepare to push " + newEmployeeArray.length + " data into elasticsearch");
        let stringFirstDay = thisDay.date.startOf("day").toString();
        let stringLastDay = thisDay.date.endOf("day").toString();
        if (history) {
            stringFirstDay = DateTime.local(1,1,1).toString();
            stringLastDay = thisDay.date.endOf("day").toString();
        }
        const querySearch = {
            "range": {
                "endDate": {
                    "gte": stringFirstDay.split("+").length > 0 ? stringFirstDay.split("+")[0] : stringFirstDay,
                    "lte": stringLastDay.split("+").length > 0 ? stringLastDay.split("+")[0] : stringLastDay
                }
            }
        }
        Utils.elasticsearchPush("notaskshour", newEmployeeArray, "id", noTaskHour, loggerNoTask, querySearch);
        history = false;
    } catch(err) {
        loggerNoTask.error(err);
    }
}

main();

const job = new cronJob("00 30 18 * * 0-5", () => {
    countTask++;
    loggerNoTask.info("Run task " + countTask);
    main();
}, null, true, "Asia/Ho_Chi_Minh");
job.start();

// const job = new cronJob("*/10 * * * *", () => {
//     countTask++;
//     loggerNoTask.info("Run task " + countTask);
//     main();
// }, null, false, "Asia/Ho_Chi_Minh");
// job.start();

