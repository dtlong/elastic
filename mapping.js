module.exports = {
    noTaskHour: {
        properties: {
            "noTaskHour": {
                type: 'float'
            }
        }
    },
    workingTime: {
        properties: {
            "commonHour": {
                type: 'float'
            }
        }
    },
    workBlocks: {
        properties: {
            "hours": {
                type: 'float'
            },
            "type": {
                type: 'keyword'
            }
        }
    },
    projectInfo: {
        properties: {
            "totalMonth": {
                type: 'float'
            },
            "totalBudget": {
                type: 'float'
            },
            "realMonth": {
                type: 'float'
            },
            "expense": {
                type: 'float'
            },
            "warranty": {
                type: 'float'
            }
        }
    }
}