const myLoggers = require('log4js');

myLoggers.configure({
    appenders: {
        mylogger: { type:"file", filename: "./log.txt" },
        loggerWBs: { type:"file", filename: "./logWB.txt" },
        loggerProj: { type:"file", filename: "./logProj.txt" },
        loggerNoTask: { type:"file", filename: "./logNoTask.txt" }
    },
    categories: {
        default: { appenders:["mylogger"], level:"ALL" },
        cateWBs: { appenders:["loggerWBs"], level:"ALL" },
        cateProj: { appenders:["loggerProj"], level:"ALL" },
        cateNoTask: { appenders:["loggerNoTask"], level:"ALL" }
    }
});
const logger = myLoggers.getLogger("default");
const loggerWBs = myLoggers.getLogger("cateWBs");
const loggerProj = myLoggers.getLogger("cateProj");
const loggerNoTask = myLoggers.getLogger("cateNoTask");

module.exports = {
    logger: logger,
    loggerWBs: loggerWBs,
    loggerProj: loggerProj,
    loggerNoTask: loggerNoTask
}