// const cron = require("node-cron");
const cronJob = require("cron").CronJob;
const { loggerProj } = require("./logger");

const { headers, url, client, DEFAULT_NAME } = require("./constants");
const { projectInfo } = require("./mapping");
const Utils = require("./request");

const optionsProInf = Utils.makeBaseOption("ProjectInfo/GetAllProjectInfoList");
const optionsEmpInf = Utils.makeBaseOption("Employee/GetAllFullEmployee");
const optionsFieldConf = Utils.makeBaseOption("FieldConfig/GetAll");
let countTask = 0;
// Usage:

async function main() {
    try {
        let resProInf = await Utils.doRequest(optionsProInf);
        let resEmpInf = await Utils.doRequest(optionsEmpInf);
        let resFieldConf = await Utils.doRequest(optionsFieldConf);
        let fullProjectInfo = JSON.parse(resProInf);
        let empInfo = JSON.parse(resEmpInf);
        let fieldConf = JSON.parse(resFieldConf);
        const projectTypeList = fieldConf.find(o => o.fieldName === "ProjectTypeID");
        const projectRoleList = fieldConf.find(o => o.fieldName === "ProjectRoleID");
        fullProjectInfo.map(obj => {
            const indexProjectType = projectTypeList ?
                projectTypeList.fieldDataValues.findIndex(o => o.id === obj.projectTypeID) :
                -1;
            indexProjectType >= 0 ?
                obj.projectTypeName = projectTypeList.fieldDataValues[indexProjectType].value :
                obj.projectTypeName = DEFAULT_NAME;

            obj.features.forEach(feature => {
                feature.members.map(emp => {
                    const indexEmpFound = empInfo.findIndex(o => o.employeeID === emp.employeeID);
                    indexEmpFound >= 0 ? emp.employeeName = empInfo[indexEmpFound].englishName : emp.employeeName = DEFAULT_NAME;
                });
            });
            obj.roles.map(role => {
                const indexProjectRole = projectRoleList ?
                    projectRoleList.fieldDataValues.findIndex(o => o.id === role.roleID) :
                    -1;
                indexProjectRole >= 0 ?
                    role.roleName = projectRoleList.fieldDataValues[indexProjectRole].value :
                    role.roleName = DEFAULT_NAME;

                const indexEmpRole = empInfo.findIndex(o => o.employeeID === role.employeeID);
                indexEmpRole >= 0 ? role.employeeName = empInfo[indexEmpRole].englishName : role.employeeName = DEFAULT_NAME;
            });

            const indexRequester = empInfo.findIndex(o => o.employeeID === obj.requesterID);
            indexRequester >= 0 ? obj.requesterName = empInfo[indexRequester].englishName : obj.requesterName = DEFAULT_NAME;
        });
        loggerProj.info("Prepare to push " + fullProjectInfo.length + " data into elasticsearch");
        const querySearch = {
            "match_all": {}
        }
        Utils.elasticsearchPush("projectinfolist", fullProjectInfo, "id", projectInfo, loggerProj, querySearch);
    } catch(err) {
        loggerProj.error(err);
    }
}


// cron.schedule("*/10 * * * *", () => {
//     countTask++;
//     loggerProj.info("Run task " + countTask);
//     main();
// });
const job = new cronJob("0 */1 * * *", () => {
    countTask++;
    loggerProj.info("Run task " + countTask);
    main();
}, null, true, "Asia/Ho_Chi_Minh");
job.start();