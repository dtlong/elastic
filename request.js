const { client } = require('./constants');
const { HEADERS, URL } = require("./constants");
const request = require('request');
const { DateTime } = require('luxon');
const pushData = async function (index, data, fieldId, logger) {
    if (typeof data === 'object' && data.length && data.length > 0) {
        data.forEach(async (obj) => {
            try {
                await client.index({
                    index: index,
                    id: obj[fieldId],
                    body: obj
                });
            } catch (err) {
                logger.error("----------------");
                logger.error(err);
                logger.error("----------------");
            }
        });
    } else {
        try {
            await client.index({
                index: index,
                id: data[fieldId],
                body: data
            });
        } catch (err) {
            logger.error("----------------");
            logger.error(err);
            logger.error("----------------");
        }
    }
}

const pushDataBulk = async function (index, data, fieldId, logger, querySearch = null) {
    try {
        let split = data.length / 1000;
        let countSplit = 0;
        while (split >= 0) {
            let dataPush = [];
            if (split === 0) {
                dataPush = data.slice(countSplit*1000, data.length);
            } else {
                dataPush = data.slice(countSplit*1000, (countSplit+1)*1000);
            }
            const dataset = dataPush.flatMap(doc => [{ index: { _index: index, _id: doc[fieldId] } }, doc]);
            const { body: bulkResponse } = await client.bulk({ refresh: true, body: dataset });
            if (bulkResponse.errors) {
                const erroredDocuments = []
                // The items array has the same order of the dataset we just indexed.
                // The presence of the `error` key indicates that the operation
                // that we did for the document has failed.
                bulkResponse.items.forEach((action, i) => {
                    const operation = Object.keys(action)[0];
                    if (action[operation].error) {
                        erroredDocuments.push({
                            // If the status is 429 it means that you can retry the document,
                            // otherwise it's very likely a mapping error, and you should
                            // fix the document before to try it again.
                            status: action[operation].status,
                            error: action[operation].error,
                            operation: body[i * 2],
                            document: body[i * 2 + 1]
                        });
                    }
                })
                logger.error(erroredDocuments);
            }
            countSplit++;
            split--;
            logger.info("Push successfully " + countSplit + " dataset");
        }
        const deletedDataNumber = await checkDataElastic(index, data, fieldId, querySearch);
        logger.warn("Need to delete " + deletedDataNumber + " data");
        const { body: count } = await client.count({ index: index })
        logger.info("Push successfully " + count.count + " data");
    } catch (err) {
        logger.error("----------------");
        logger.error(err);
        logger.error("----------------");
    }
}

const checkDataElastic = async function (index, data, fieldId, querySearch = null) {
    if (querySearch === null || querySearch === undefined) {
        return 0;
    }
    let listData = await getResultsFromElastic(index, querySearch);
    let deleteCount = 0;
    for (let i = listData.length - 1; i >= 0; i--) {
        if (!data.some(o => o[fieldId] === listData[i]._id)) {
            const { body } = await client.delete({
                id: listData[i]._id,
                index: index
            });
            deleteCount++;
            listData.splice(i, 1);
        }
    }
    return deleteCount;
}

const getResultsFromElastic = async function (index, querySearch) {
    let responseAll = [];
    const responseQueue = [];

    searchQuery = {
        index: index,
        body: {
            query: querySearch
        }
    }
    searchQuery.scroll = '10s';
    searchQuery.size = 10000;

    responseQueue.push(await client.search(searchQuery));

    while (responseQueue.length) {
        const response = responseQueue.shift();
        const body = response.body;

        responseAll = responseAll.concat(body.hits.hits);

        if (body.hits.total.value == responseAll.length) {
            break;
        }

        // get the next response if there are more to fetch
        responseQueue.push(
            await client.scroll({
                scrollId: body._scroll_id,
                scroll: '30s'
            })
        );
    }

    return responseAll;
}

module.exports = {
    getFirstorLastDayofWeek: function (type) {
        let date = DateTime.local();
        let firstOrLastDay = DateTime.local();
        if (type === "last") {
            firstOrLastDay = date.plus({ days: 7 - date.weekday });
        } else if (type === "first") {
            firstOrLastDay = date.minus({ days: date.weekday - 1 });
        } else if (type === "lastWeek") {
            firstOrLastDay = date.minus({ days: date.weekday });
        }
        return {
            text: firstOrLastDay.year + "-" + firstOrLastDay.month + "-" + firstOrLastDay.day,
            date: firstOrLastDay
        };
    },
    getDateNowObj: function () {
        let date = DateTime.local();
        return date.year + "-" + date.month + "-" + date.day;
    },
    makeBaseOption: function (path, body = null, method = "POST") {
        return {
            url: URL + path,
            method: method,
            headers: HEADERS,
            body: !!body && JSON.stringify(body)
        }
    },
    doRequest: function (options) {
        return new Promise(function (resolve, reject) {
            request(options, function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    resolve(body);
                } else {
                    reject(error);
                }
            });
        });
    },
    elasticsearchPush: async function (index, data, fieldId, mappingData, logger, querySearch = null) {
        const exist = await client.indices.exists({
            index: index
        });
        if (exist.body) {
            pushDataBulk(index, data, fieldId, logger, querySearch);
        } else {
            const result = await client.indices.create({
                index: index
            });
            if (result.body.acknowledged) {
                await client.indices.putMapping({
                    index: index,
                    body: mappingData
                });
                pushDataBulk(index, data, fieldId, logger);
            }
        }
    },
    getWbType: function (type) {
        switch (type) {
            case 0:
                return "Work";
            case 1:
                return "Leave";
            case 2:
                return "OT";
            case 3:
                return "Business Trip";
            case 4:
                return "Meeting";
            default:
                return "Other";
        }
    }
}