
const { headers, url, client, DEFAULT_NAME, MIN_DATE_OBJ } = require("./constants");
const { loggerWBs } = require("./logger");
const { workBlocks } = require("./mapping");
const Utils = require("./request");
const optionsProInf = Utils.makeBaseOption("ProjectInfo/GetAllProjectInfoList");
const optionsEmpInf = Utils.makeBaseOption("Employee/GetAllFullEmployee");
const optionsFieldConf = Utils.makeBaseOption("FieldConfig/GetAll");
const optionsCompGroup = Utils.makeBaseOption("CompanyGroup/GetFlatCompanyGroupsVersion2", {});

const { DateTime } = require('luxon');

let countTask = 0;
// Usage:

async function main() {
    try {
        const body = {
            "fromDate": MIN_DATE_OBJ,
            "toDate": "2020-5-3",
            "originalGroupID": [],
            "projectID": null,
            "featureID": null,
            "workTypeID": null
        };
        
        const optionsWb = Utils.makeBaseOption("WorkBlock/GetWorkBlocks", body);
        let resWb = await Utils.doRequest(optionsWb);
        let resProInf = await Utils.doRequest(optionsProInf);
        let resEmpInf = await Utils.doRequest(optionsEmpInf);
        let resFieldConf = await Utils.doRequest(optionsFieldConf);
        let resCompGroup = await Utils.doRequest(optionsCompGroup);

        let response = JSON.parse(resWb);
        const projectInfo = JSON.parse(resProInf);
        const empInfo = JSON.parse(resEmpInf);
        const fieldConf = JSON.parse(resFieldConf);
        const workTypeList = fieldConf.find(o => o.fieldName === "WorkTypeID");
        const compGroup = JSON.parse(resCompGroup);

        response.wBlocks.map(obj => {
            obj.type = Utils.getWbType(obj.type);
            const indexEmpFound = empInfo.findIndex(o => o.employeeID === obj.reporterID);
            indexEmpFound >= 0 ? obj.reportName = empInfo[indexEmpFound].englishName : obj.reportName = DEFAULT_NAME;

            const indexAssigneeFound = empInfo.findIndex(o => o.employeeID === obj.assigneeID);
            indexAssigneeFound >= 0 ? obj.assigneeName = empInfo[indexAssigneeFound].englishName : obj.assigneeName = DEFAULT_NAME;
 
            const indexConfig = workTypeList ? workTypeList.fieldDataValues.findIndex(o => o.id === obj.workTypeID) : -1;
            indexConfig >= 0 ? obj.workTypeName = workTypeList.fieldDataValues[indexConfig].value : obj.workTypeName = DEFAULT_NAME;

            const project = projectInfo.find(o => o.originalID === obj.originalProjectID);
            if (project) {
                obj.projectName = project.name;
                obj.projectHqName = project.hqName;
                const indexFeature = project.features.findIndex(o => o.id === obj.featureID);
                indexFeature >= 0 ? obj.featureName = project.features[indexFeature].name : obj.featureName = DEFAULT_NAME;
            } else {
                obj.projectName = DEFAULT_NAME;
                obj.projectHqName = DEFAULT_NAME;
                obj.featureName = DEFAULT_NAME;
            }
            const indexCompGroup = compGroup ?
                compGroup.companyGroups.findIndex(o => o.employees.some(emp => emp.employeeID === obj.assigneeID)) :
                -1;
            if (indexCompGroup >= 0) {
                const groupInfo = compGroup.companyGroups[indexCompGroup];
                if (groupInfo.levelName.toLowerCase() === "part") {
                    obj.partName = groupInfo.groupName;
                    const indexTeam = compGroup.companyGroups.findIndex(o => o.oldGroupID === groupInfo.parentGroupID);
                    if (indexTeam >= 0) {
                        obj.teamName = compGroup.companyGroups[indexTeam].groupName;
                    } else {
                        obj.teamName = DEFAULT_NAME;
                    }
                } else if (groupInfo.levelName.toLowerCase() === "team") {
                    obj.partName = DEFAULT_NAME;
                    obj.teamName = groupInfo.groupName;
                } else {
                    obj.partName = "";
                    obj.teamName = "";
                }
            } else {
                obj.partName = "";
                obj.teamName = "";
            }
        });
        loggerWBs.info("Prepare to push " + response.wBlocks.length + " data into elasticsearch");
        const stringFirstDay = DateTime.local(1,1,1).toString();
        const stringLastDay = DateTime.local(2020, 5, 3).endOf("day").toString();
        const querySearch = {
            "range": {
                "endDate": {
                    "gte": stringFirstDay.split("+").length > 0 ? stringFirstDay.split("+")[0] : stringFirstDay,
                    "lte": stringLastDay.split("+").length > 0 ? stringLastDay.split("+")[0] : stringLastDay
                }
            }
        }
        Utils.elasticsearchPush("weeklyworkblocks", response.wBlocks, "id", workBlocks, loggerWBs, querySearch);
    } catch(err) {
        loggerWBs.error(err);
    }
}


main();