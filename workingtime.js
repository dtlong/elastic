const cron = require("node-cron");
const { logger } = require("./logger");

const { headers, url, client, DEFAULT_NAME } = require("./constants");
const { workingTime } = require("./mapping");
const Utils = require("./request");
const body = {
    "fromDate": "2020-01-01",
    "toDate": "2020-04-06",
    "originalGroupID": [],
    "projectID": null,
    "featureID": null,
    "workTypeID": null
};

const optionsWb = Utils.makeBaseOption("WorkBlock/GetWorkBlocks", body);
const optionsEmpInf = Utils.makeBaseOption("Employee/GetAllFullEmployee");
const optionsCompGroup = Utils.makeBaseOption("CompanyGroup/GetFlatCompanyGroupsVersion2", {});
let countTask = 0;
  
// Usage:
async function main() {
    try {
        let resWb = await Utils.doRequest(optionsWb);
        let resEmpInf = await Utils.doRequest(optionsEmpInf);
        let resCompGroup = await Utils.doRequest(optionsCompGroup);
        let response = JSON.parse(resWb);
        let empInfo = JSON.parse(resEmpInf);
        const compGroup = JSON.parse(resCompGroup);
        let newEmployeeArray = [];

        response.employeeIDs.map(obj => {
            const indexEmpFound = empInfo.findIndex(o => o.employeeID === obj);
            const listWbReport = response.wBlocks.filter(o => o.assigneeID === obj && (o.type === 0 || o.type === 2));
            let dayWb = [];
            let partName = DEFAULT_NAME;
            let teamName = DEFAULT_NAME;
            listWbReport && listWbReport.forEach(wb => {
                const date = new Date(wb.endDate);
                const objectDate = {
                    date: date.getDate(),
                    month: date.getMonth(),
                    year: date.getFullYear()
                }
                if (!dayWb.some(o => o.date === objectDate.date && o.month === objectDate.month && o.year === objectDate.year)) {
                    dayWb.push(objectDate);
                }
            });
            const reducer = (accumulator, currentValue) => accumulator + currentValue.hours;
            const totalHour = listWbReport && listWbReport.length > 0 && listWbReport.reduce(reducer, 0);

            const indexCompGroup = compGroup ?
                compGroup.companyGroups.findIndex(o => o.employees.some(emp => emp.employeeID === obj)) :
                -1;
            if (indexCompGroup >= 0) {
                const groupInfo = compGroup.companyGroups[indexCompGroup];
                if (groupInfo.levelName.toLowerCase() === "part") {
                    partName = groupInfo.groupName;
                    const indexTeam = compGroup.companyGroups.findIndex(o => o.oldGroupID === groupInfo.parentGroupID);
                    if (indexTeam >= 0) {
                        teamName = compGroup.companyGroups[indexTeam].groupName;
                    }
                } else if (groupInfo.levelName.toLowerCase() === "team") {
                    teamName = groupInfo.groupName;
                }
            }
            
            if (indexEmpFound >= 0) {
                newEmployeeArray.push({
                    id: obj,
                    name: empInfo[indexEmpFound] ? empInfo[indexEmpFound].englishName : "",
                    partName: partName,
                    teamName: teamName,
                    workingDays: parseInt(dayWb.length),
                    commonHour: dayWb.length > 0 && typeof totalHour === 'number' ? (totalHour) / (dayWb.length) : 0
                });
            }
        });
        logger.info("Prepare to push " + newEmployeeArray.length + " data into elasticsearch");
        Utils.elasticsearchPush("workingdata", newEmployeeArray, "id", workingTime, logger);
    } catch(err) {
        logger.error(err);
    }
}

cron.schedule("*/1 * * * *", () => {
    countTask++;
    logger.info("Run task " + countTask);
    main();
});
